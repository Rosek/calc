<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use App\Model\Calculate;

class CalcController extends AbstractController
{
    public function calc(Request $request) 
    {
        $calc = new Calculate();

        $form = $this->createFormBuilder($calc)
            ->add('number1', NumberType::class)
            ->add('number2', NumberType::class)
            ->add('send', SubmitType::class, ['label' => 'Calculate'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calc = $form->getData();
            $calc->calculate();

            return $this->render('result.form.html.twig', [
                'calc' => $calc,
            ]);
            //return $this->redirectToRoute('task_success');
        }

        return $this->render('calc.form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
