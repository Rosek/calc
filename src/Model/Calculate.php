<?php

namespace App\Model;

class Calculate
{
    private $number1;
    private $number2;
    private $result;

    /**
     * Get the value of number1
     */ 
    public function getNumber1()
    {
        return $this->number1;
    }

    /**
     * Set the value of number1
     *
     * @return  self
     */ 
    public function setNumber1($number1)
    {
        $this->number1 = $number1;

        return $this;
    }

    /**
     * Get the value of number2
     */ 
    public function getNumber2()
    {
        return $this->number2;
    }

    /**
     * Set the value of number2
     *
     * @return  self
     */ 
    public function setNumber2($number2)
    {
        $this->number2 = $number2;

        return $this;
    }

    /**
     * Get the value of result
     */ 
    public function getResult()
    {
        return $this->result;
    }

    public function calculate() 
    {
        $this->result = $this->number1 + $this->number2;
    }
}
